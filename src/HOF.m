classdef HOF < handle
    %   HOF Context object for computing orientation histograms of optical flow.
    %
    %   Copyright 2017 Vidrio Technologies
    %   by Nathan Clack <nathan@vidriotech.com>
    %
    %   Licensed under the Apache License, Version 2.0 (the "License");
    %   you may not use this file except in compliance with the License.
    %   You may obtain a copy of the License at
    %
    %       http://www.apache.org/licenses/LICENSE-2.0
    properties(Hidden,SetAccess=immutable)
        handle

        inputSize
        cellSize
        nbins
        sigma_derivative
        sigma_smoothing
    end
    
    methods
        function obj=HOF(inputSize,cellSize,nbins,sigma_derivative,sigma_smoothing)
            %% obj=HOF(inputSize,cellSize,nbins,sigma_derivative,sigma_smoothing)
            obj.inputSize=uint32(inputSize(:));
            obj.cellSize=uint32(cellSize(:));
            obj.nbins=uint32(nbins);
            obj.sigma_derivative=single(sigma_derivative(:));
            obj.sigma_smoothing=single(sigma_smoothing(:));
            obj.handle=hof_init(obj.inputSize,obj.cellSize,obj.nbins,obj.sigma_derivative,obj.sigma_smoothing);
        end
        
        function delete(obj)
            %% delete(obj)
            if ~isempty(obj.handle)
                hof_destroy(obj.handle);
            end
        end
        
        function out=compute(obj,im)
            %% out=compute(obj,im)
            %
            % The optical flow is computed relative to the last
            % image passed to compute.  For the first image, 
            % the 'previous' image is just a field of 0.
            out=hof_compute(obj.handle,im);
        end

        function h=show(obj,I,varargin)
            %% h=show(obj,I)
            %  h=show(obj,I,LineStyle)
            %
            % Compute features for the input image, I, and generates a 
            % plot to visualize the result.
            %
            % Returns handles to the generated line objects.
            feat=obj.compute(I);            
            imshow(I,[]);
            h=vis(feat,double(obj.cellSize(:)),varargin{:});            
        end  
    end

    methods(Static)
        function v=mexVersion()
            %% v=mexVersion()
            %  returns the version identifier for this API/SDK
            %  This is intended to be useful for tracking issues.
            v=heffalump_version();
        end
        
        function demo()
            Iprev=imread('cameraman.tif');
            hof=Heffalump.HOF(size(I),[8,8],8);
            hof.compute(Iprev);            
            for i=1:100
                dr=rand(1,2)*10;
                Inext=imtranslate(Iprev,dr,'FillValues',I(1));                
                hof.show(I2,'linewidth',2,'color','r');
                drawnow;
                Iprev=Inext;
            end
        end
    end
end

%{ 
    
NOTES

 - This class wraps the mex functions and provides a public interface.
   The mex functions are more-or-less a private interface; they get
   installed to a private folder that this class can reference.
%}
