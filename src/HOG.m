classdef HOG < handle
    % HOG Context object for computing orientation histograms of image gradient.
    
    %   Copyright 2017 Vidrio Technologies
    %   by Nathan Clack <nathan@vidriotech.com>
    %
    %   Licensed under the Apache License, Version 2.0 (the "License");
    %   you may not use this file except in compliance with the License.
    %   You may obtain a copy of the License at
    %
    %       http://www.apache.org/licenses/LICENSE-2.0
    properties(Hidden,SetAccess=immutable)
        handle
        
        inputSize
        cellSize
        nbins
    end
    
    methods
        function obj=HOG(inputSize,cellSize,nbins)
            %% obj=HOG(inputSize,cellSize,nbins)
            obj.inputSize=uint32(inputSize(:));
            obj.cellSize=uint32(cellSize(:));
            obj.nbins=uint32(nbins(:));
            obj.handle=hog_init(obj.inputSize,obj.cellSize,obj.nbins);
        end
        
        function delete(obj)
            %% delete(obj)
            if ~isempty(obj.handle)
                hog_destroy(obj.handle);
            end
        end
        
        function out=compute(obj,im)
            %% out=compute(obj,im)
            out=hog_compute(obj.handle,im);
        end
                
        function h=show(obj,I,varargin)
            %% h=show(obj,I)
            %  h=show(obj,I,LineStyle)
            %
            % Compute features for the input image, I, and generates a 
            % plot to visualize the result.
            %
            % Returns handles to the generated line objects.
            feat=obj.compute(I);            
            imshow(I,[]);
            h=vis(feat,double(obj.cellSize(:)),varargin{:});            
        end        
    end
    
    methods(Static)
        function v=mexVersion()
            %% v=mexVersion()
            %  returns the version identifier for this API/SDK
            %  This is intended to be useful for tracking issues.
            v=heffalump_version();
        end
        
        function demo()
            I=imread('cameraman.tif');
            hog=Heffalump.HOG(size(I),[8,8],8);
            hog.show(I,'linewidth',2,'color','r');
        end
    end
end

%{
    
NOTES

 - This class wraps the mex functions and provides a public interface.
   The mex functions are more-or-less a private interface; they get
   installed to a private folder that this class can reference.

%}
