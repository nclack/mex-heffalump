classdef LucasKanade < handle
    % LucasKanade Compute context for optical flow using Lucas-Kanade

    %   Copyright 2017 Vidrio Technologies
    %   by Nathan Clack <nathan@vidriotech.com>
    %
    %   Licensed under the Apache License, Version 2.0 (the "License");
    %   you may not use this file except in compliance with the License.
    %   You may obtain a copy of the License at
    %
    %       http://www.apache.org/licenses/LICENSE-2.0
    properties(Hidden,SetAccess=immutable)
        handle

        inputSize
        sigma_derivative
        sigma_smoothing
    end
    
    methods
        function obj=LucasKanade(inputSize,sigma_derivative,sigma_smoothing)
            %% obj=LucasKanade(inputSize,sigma_derivative,sigma_smoothing)
            obj.inputSize=uint32(inputSize(:));
            obj.sigma_derivative=single(sigma_derivative(:));
            obj.sigma_smoothing=single(sigma_smoothing(:));
            obj.handle=lk_init(obj.inputSize,obj.sigma_derivative,obj.sigma_smoothing);
        end
        
        function delete(obj)
            %% delete(obj)
            if ~isempty(obj.handle)
                lk_destroy(obj.handle);
            end
        end
        
        function out=compute(obj,im)
            %% out=compute(obj,im)
            %
            % The optical flow is computed relative to the last
            % image passed to compute.  For the first image, 
            % the 'previous' image is just a field of 0.
            out=lk_compute(obj.handle,im);
        end
    end

    methods(Static)
        function v=mexVersion()
            %% v=mexVersion()
            %  returns the version identifier for this API/SDK
            %  This is intended to be useful for tracking issues.
            v=heffalump_version();
        end

        function demo()
            Iprev=imread('cameraman.tif');
            lk=Heffalump.LucasKanade(size(I),1,5);
            compute(lk,Iprev);            
            for i=1:100
                dr=rand(1,2)*5;
                Inext=imtranslate(Iprev,dr,'FillValues',I(1));                
                f=compute(lk,I2);
                quiver(f(:,:,1),f(:,:,2));
                drawnow;
                Iprev=Inext;
            end
        end
    end
end

%{ 
    
NOTES

 - This class wraps the mex functions and provides a public interface.
   The mex functions are more-or-less a private interface; they get
   installed to a private folder that this class can reference.
%}
