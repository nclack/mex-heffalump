//   Copyright 2017 Vidrio Technologies
//   by Nathan Clack <nathan@vidriotech.com>
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0

#include <mex.h>
#include <heffalump/version.h>
#include "logger.h" // CHECK macro, error reporting

static int validate_input(int nrhs, const mxArray *prhs[]) {
    CHECK(nrhs==0);
    return 1;
Error:
    return 0;
}

static int validate_output(int nlhs, const mxArray *plhs[]) {
    CHECK(nlhs==1);
    return 1;
Error:
    return 0;
}


void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
    CHECK(validate_input(nrhs,prhs));
    CHECK(validate_output(nlhs,plhs));

    plhs[0]=mxCreateString(heffalump_api_version());
Error:;
}