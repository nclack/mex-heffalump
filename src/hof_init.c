//   Copyright 2017 Vidrio Technologies
//   by Nathan Clack <nathan@vidriotech.com>
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0

#include <mex.h>
#include <heffalump/hof.h>
#include "logger.h" // CHECK macro, logger (used for error reporting)

static int validate_input_size(const mxArray *a) {
    CHECK(mxGetClassID(a)==mxUINT32_CLASS);
    const mwSize* dims=mxGetDimensions(a);
    CHECK(dims[0]==2);
    CHECK(dims[1]==1);
    return 1;
Error:
    return 0;
}

static int validate_cell_size(const mxArray *a) {
    CHECK(mxGetClassID(a)==mxUINT32_CLASS);
    const mwSize* dims=mxGetDimensions(a);
    CHECK(dims[0]==2);
    CHECK(dims[1]==1);
    return 1;
Error:
    return 0;
}

static int validate_nbins(const mxArray *a) {
    CHECK(mxGetClassID(a)==mxUINT32_CLASS);
    const mwSize* dims=mxGetDimensions(a);
    CHECK(dims[0]==1);
    CHECK(dims[1]==1);
    return 1;
Error:
    return 0;
}

static int validate_single_val(const mxArray *a) {
    CHECK(mxGetClassID(a)==mxSINGLE_CLASS);
    const mwSize* dims=mxGetDimensions(a);
    CHECK(dims[0]==1);
    CHECK(dims[1]==1);
    return 1;
Error:
    return 0;
}

static int validate_input(int nrhs, const mxArray *prhs[]) {
    CHECK(nrhs==5);
    CHECK(validate_input_size(prhs[0]));
    CHECK(validate_cell_size(prhs[1]));
    CHECK(validate_nbins(prhs[2]));
    CHECK(validate_single_val(prhs[3]));
    CHECK(validate_single_val(prhs[4]));
    return 1;
Error:
    return 0;
}

static int validate_output(int nlhs, const mxArray *plhs[]) {
    CHECK(nlhs==1);
    return 1;
Error:
    return 0;
}

// Allocates nbytes contained in a UINT8 mxArray
static mxArray* array_malloc(size_t nbytes) {
    mwSize dims[]={nbytes,1};
    return mxCreateNumericArray(2,dims,mxUINT8_CLASS,mxREAL);
}


void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
    CHECK(validate_input(nrhs,prhs));
    CHECK(validate_output(nlhs,plhs));
    uint32_t *imsz,*cellsz,*nbins;
    float *der,*smo;
    
    imsz  =mxGetData(prhs[0]); // expect this is (#row,#col)
    cellsz=mxGetData(prhs[1]);
    nbins =mxGetData(prhs[2]);
    der   =mxGetData(prhs[3]);
    smo   =mxGetData(prhs[4]);

    struct HOFParameters params={
        .lk={.sigma={.derivative=*der,.smoothing=*smo}},
        .cell={.w=cellsz[0],.h=cellsz[1]},
        .input={.w=imsz[0],.h=imsz[1],.pitch=imsz[0]},
        .nbins=*nbins
    };
    struct HOFContext ctx=HOFInitialize(logger,params);    
    plhs[0]=array_malloc(sizeof(struct HOFContext));
    struct HOFContext* out=mxGetData(plhs[0]);
    *out=ctx;
Error:;
}

/* NOTES:

row-major vs col-major transposition       
    - first dimension is always the one with the smallest stride (should be 1).
    - this may mean the concept of "width" and "height" is transposed moving from matlab.
    - Since the smallest dimension moves along the rows, the "width" of the image is the "number of rows"
      Have to imagine things transposed.

*/