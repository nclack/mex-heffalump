//   Copyright 2017 Vidrio Technologies
//   by Nathan Clack <nathan@vidriotech.com>
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0

#include <mex.h>
#include <heffalump/hog.h>
#include "logger.h" // CHECK macro, logger (used for error reporting)

static int validate_context(const mxArray* a) {
    CHECK(mxGetClassID(a)==mxUINT8_CLASS);
    const mwSize* dims=mxGetDimensions(a);
    CHECK(dims[0]==sizeof(struct HOGContext));
    CHECK(dims[1]==1);
    return 1;
Error:
    return 0;
}

static int validate_input_image(const mxArray* a,size_t w,size_t h) {
    CHECK(mxGetNumberOfDimensions(a)==2);
    const mwSize* dims=mxGetDimensions(a);
    CHECK(dims[0]==w);
    CHECK(dims[1]==h);
    return 1;
Error:
    return 0;
}

static int validate_input(int nrhs, const mxArray *prhs[]) {
    CHECK(nrhs==2);
    CHECK(validate_context(prhs[0]));
    struct HOGContext *ctx=mxGetData(prhs[0]);
    CHECK(validate_input_image(prhs[1],ctx->w,ctx->h));
    return 1;
Error:
    return 0;
}

static int validate_output(int nlhs, const mxArray *plhs[]) {
    CHECK(nlhs==1);
    return 1;
Error:
    return 0;
}

static void map_image(struct HOGImage *im, const mxArray* a) {
    const mwSize* dims=mxGetDimensions(a);
        im->buf=mxGetData(a);
    im->w=dims[0];
    im->h=dims[1];
    im->pitch=dims[0];
    switch(mxGetClassID(a)) {
        case mxUINT8_CLASS:  im->type=hog_u8; break;
        case mxUINT16_CLASS: im->type=hog_u16; break;
        case mxUINT32_CLASS: im->type=hog_u32; break;
        case mxUINT64_CLASS: im->type=hog_u64; break;
        case mxINT8_CLASS:   im->type=hog_i8; break;
        case mxINT16_CLASS:  im->type=hog_i16; break;
        case mxINT32_CLASS:  im->type=hog_i32; break;
        case mxINT64_CLASS:  im->type=hog_i64; break;
        case mxSINGLE_CLASS: im->type=hog_f32; break;
        case mxDOUBLE_CLASS: im->type=hog_f64; break;
    }
}

/// Determines the output dimensional ordering depending on
/// the reported strides.
static void map_dims(mwSize *dims,const struct HOGFeatureDims *shape,const struct HOGFeatureDims *strides) {
    // 3-element sorting network to sort dims according to strides.
    // dims[0] will have the smallest-stride dimension
    int idim[3]={0,1,2}; // dim index for x,y,nbins
    uint64_t args[3] ={strides->x,strides->y,strides->bin};
    #define swap_(T,a,b) do{T t=(a);(a)=(b);(b)=t;}while(0)
    #define swap(i,j)    do{swap_(int,idim[i],idim[j]); swap_(mwSize,args[i],args[j]);}while(0)
    if(args[0]>args[1]) swap(0,1);
    if(args[0]>args[2]) swap(0,2);
    if(args[1]>args[2]) swap(1,2);
    #undef swap_
    #undef swap
    dims[idim[0]]=shape->x;
    dims[idim[1]]=shape->y;
    dims[idim[2]]=shape->bin;
}
/*  Example
    x y n (strides)
    3 2 1 -> 2 3 1 -> 1 3 2 -> 1 2 3
    i j k (dim index)
    0 1 2 -> 1 0 2 -> 2 0 1 -> 2 1 0 --  i.e. dim 2 is `x`, dim 1 is `y`, and dim 0 is `nbins`  
*/

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
    CHECK(validate_input(nrhs,prhs));
    CHECK(validate_output(nlhs,plhs));
    struct HOGContext *ctx=mxGetData(prhs[0]);
    struct HOGImage im;
    map_image(&im,prhs[1]);    
    HOGCompute(ctx,im);

    struct HOGFeatureDims shape,strides;
    HOGOutputShape(ctx,&shape);
    HOGOutputStrides(ctx,&strides);
    mwSize dims[3];
    map_dims(dims,&shape,&strides);
    mxArray* out=mxCreateNumericArray(3,dims,mxSINGLE_CLASS,mxREAL);
    HOGOutputCopy(ctx,
        mxGetData(out),
        mxGetNumberOfElements(out)*sizeof(float));
    plhs[0]=out;
Error:;
}

/* NOTES

 - `Compute` calls may be asynchronous.
   The synchronization point is the `OutputCopy` call.
*/