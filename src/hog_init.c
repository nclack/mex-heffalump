//   Copyright 2017 Vidrio Technologies
//   by Nathan Clack <nathan@vidriotech.com>
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0

#include <mex.h>
#include <heffalump/hog.h>
#include "logger.h" // CHECK macro, logger (used for error reporting)

static int validate_input_size(const mxArray *a) {
    CHECK(mxGetClassID(a)==mxUINT32_CLASS);
    const mwSize* dims=mxGetDimensions(a);
    CHECK(dims[0]==2);
    CHECK(dims[1]==1);
    return 1;
Error:
    return 0;
}

static int validate_cell_size(const mxArray *a) {
    CHECK(mxGetClassID(a)==mxUINT32_CLASS);
    const mwSize* dims=mxGetDimensions(a);
    CHECK(dims[0]==2);
    CHECK(dims[1]==1);
    return 1;
Error:
    return 0;
}

static int validate_nbins(const mxArray *a) {
    CHECK(mxGetClassID(a)==mxUINT32_CLASS);
    const mwSize* dims=mxGetDimensions(a);
    CHECK(dims[0]==1);
    CHECK(dims[1]==1);
    return 1;
Error:
    return 0;
}

static int validate_input(int nrhs, const mxArray *prhs[]) {
    CHECK(nrhs==3);
    CHECK(validate_input_size(prhs[0]));
    CHECK(validate_cell_size(prhs[1]));
    CHECK(validate_nbins(prhs[2]));
    return 1;
Error:
    return 0;
}

static int validate_output(int nlhs, mxArray *plhs[]) {
    CHECK(nlhs==1);
    return 1;
Error:
    return 0;
}

// Allocates nbytes contained in a UINT8 mxArray
static mxArray* array_malloc(size_t nbytes) {
    mwSize dims[]={nbytes,1};
    return mxCreateNumericArray(2,dims,mxUINT8_CLASS,mxREAL);
}


void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
    CHECK(validate_input(nrhs,prhs));
    CHECK(validate_output(nlhs,plhs));
    uint32_t *imsz,*cellsz,*nbins;
    
    imsz  =mxGetData(prhs[0]); // expect this is (#row,#col)=(#y,#x)
    cellsz=mxGetData(prhs[1]);
    nbins =mxGetData(prhs[2]);

    struct HOGParameters params={
        .cell={.w=cellsz[1],.h=cellsz[0]},
        .nbins=*nbins
    };
    struct HOGContext ctx=HOGInitialize(logger,params,imsz[0],imsz[1]);    
    plhs[0]=array_malloc(sizeof(struct HOGContext));
    struct HOGContext* out=mxGetData(plhs[0]);
    *out=ctx;
Error:;
}

/* NOTES:

row-major vs col-major transposition       
    - first dimension is always the one with the smallest stride (should be 1).
    - this may mean the concept of "width" and "height" is transposed moving from matlab.
    - Since the smallest dimension moves along the rows, the "width" of the image is the "number of rows"
      Have to imagine things transposed.

*/
