//   Copyright 2017 Vidrio Technologies
//   by Nathan Clack <nathan@vidriotech.com>
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0

#include "logger.h"
#include <stdio.h>
#include <stdarg.h>
#include <mex.h>

#if 1 // toggle this to turn on and off debug messages
#define LOG(...) logger(0,__FILE__,__LINE__,__FUNCTION__,__VA_ARGS__)
#else
#define LOG(...)
#endif

#define ERR(...) logger(1,__FILE__,__LINE__,__FUNCTION__,__VA_ARGS__)
#define CHECK(e) do {if(!((e))) {ERR("Expression was false:\n\t%s\n",#e); goto Error;}} while(0)

#ifdef _MSC_VER
#define snprintf _snprintf // windows posix compatibility
#endif

void logger(int is_error,const char *file,int line,const char* function,const char *fmt,...) {
    char buf1[1024]={0},
    buf2[1024]={0};
    va_list ap;
    va_start(ap,fmt);
    vsnprintf(buf1,sizeof(buf1),fmt,ap);
    va_end(ap);
    #if 1 // toggle this to turn on/off file and line info with debug and error messages
        snprintf(buf2,sizeof(buf2),"%s(%d): %s()\n\t - %s\n",file,line,function,buf1);
    #else
        snprintf(buf2,sizeof(buf2),"%s\n",buf1);
    #endif
    if(is_error) {
        mexErrMsgIdAndTxt("Heffalump:MexError",buf2); // this can throw an exception
    } else
        mexWarnMsgIdAndTxt("Heffalump:MexWarning",buf2);
}
