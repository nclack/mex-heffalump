//   Copyright 2017 Vidrio Technologies
//   by Nathan Clack <nathan@vidriotech.com>
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0

#pragma once
#ifndef H_NGC_MEX_HEFFALUMP_LOGGER
#define H_NGC_MEX_HEFFALUMP_LOGGER

// This gets included in other c files
// It defines a few private helper functions/macros that are used for error handling

#if 1 // toggle this to turn on and off debug messages
#define LOG(...) logger(0,__FILE__,__LINE__,__FUNCTION__,__VA_ARGS__)
#else
#define LOG(...)
#endif

#define ERR(...) logger(1,__FILE__,__LINE__,__FUNCTION__,__VA_ARGS__)
#define CHECK(e) do {if(!((e))) {ERR("Expression was false:\n\t%s\n",#e); goto Error;}} while(0)

#ifdef __cplusplus
extern "C" {
#endif    

    void logger(int is_error,const char *file,int line,const char* function,const char *fmt,...);

#ifdef __cplusplus
}
#endif

#endif
