function out=tests
    %  out=tests()
    
    %   Copyright 2017 Vidrio Technologies
    %   by Nathan Clack <nathan@vidriotech.com>
    %
    %   Licensed under the Apache License, Version 2.0 (the "License");
    %   you may not use this file except in compliance with the License.
    %   You may obtain a copy of the License at
    %
    %       http://www.apache.org/licenses/LICENSE-2.0

    out=functiontests(localfunctions);
end

function testInitAndDestroy(testCase)
    sz=[320 240];
    cellsz=[40 40];
    nbin=8;
    
    lk =Heffalump.LucasKanade(sz,1,3);
    hog=Heffalump.HOG(sz,[40 40],nbin);
    hof=Heffalump.HOF(sz,[40 40],nbin,1,3);
end

function testRandomSingles(testCase)
    sz=[320 240];
    cellsz=[40 40];
    nbin=8;
    
    lk =Heffalump.LucasKanade(sz,1,3);
    hog=Heffalump.HOG(sz,[40 40],nbin);
    hof=Heffalump.HOF(sz,[40 40],nbin,1,3);
    for i=1:10        
        im=rand(sz,'single');
        
        out=compute(lk,im);
        verifyEqual(testCase,size(out),[sz(:)' 2]);
        verifyTrue(testCase,~any(isnan(out(:))));
        
        out=compute(hog,im);
        verifyEqual(testCase,size(out),[floor(sz(:)./cellsz(:))' nbin]);
        verifyTrue(testCase,~any(isnan(out(:))));
        
        out=compute(hof,im);
        verifyEqual(testCase,size(out),[floor(sz(:)./cellsz(:))' nbin]);
        verifyTrue(testCase,~any(isnan(out(:))));
    end
end

function testScalarTypeValidation(testCase)
    shouldpass={...
        'uint8','uint16','uint32',...
        'int8','int16','int32',...
        'single'};
    shouldfail={'uint64','int64','double'};

    sz=[320 240];
    cellsz=[40 40];
    nbin=8;
    
    lk =Heffalump.LucasKanade(sz,1,3);
    hog=Heffalump.HOG(sz,cellsz,nbin);
    hof=Heffalump.HOF(sz,cellsz,nbin,1,3);

    for typeid=shouldfail
        im=zeros(sz,typeid{1});
        verifyError(testCase, @() compute(lk,im), 'Heffalump:MexError');
        verifyError(testCase, @() compute(hog,im), 'Heffalump:MexError');
        verifyError(testCase, @() compute(hof,im), 'Heffalump:MexError');
    end

    for typeid=shouldpass
        im=zeros(sz,typeid{1});
        verifyWarningFree(testCase, @() compute(lk,im), 'Heffalump:MexError');
        verifyWarningFree(testCase, @() compute(hog,im), 'Heffalump:MexError');
        verifyWarningFree(testCase, @() compute(hof,im), 'Heffalump:MexError');
    end
end

function testImageSizeValidation(testCase)    
    % Note: validation is done at the last possible moment. In this case it's in compute().
    sizes={[319,240]}; % alignment requirement
    for sz=sizes
        im=zeros(sz{1},'single');
        verifyError(testCase, @() compute(Heffalump.LucasKanade(sz{1},1,3),im), 'Heffalump:MexError');
    end

    sizes={[0,0],[1,1],[32,32],[319,240]}; % smaller than cell size, alignment requirement
    for sz=sizes
        im=zeros(sz{1},'single');        
        verifyError(testCase, @() compute(Heffalump.HOG(sz{1},[40 40],8),im), 'Heffalump:MexError');
        verifyError(testCase, @() compute(Heffalump.HOF(sz{1},[40 40],8,1,3),im), 'Heffalump:MexError');
    end
end