function h=vis(feat,cellsize,varargin)
%% Visualize HOG features.
%
%  h=vis(feat,cellsize)
%  h=vis(feat,cellsize,Prop,Value)
%
%  feat
%       an [NxMxB] array with B bins that span angles from 0 to 2*pi
%  cellsize
%       the area covered by single cellin pixels. e.g. [16x16]
%  scale
%       multiplied times the magnitude in each bin to determine how long
%       to draw the line
%  Prop,Value
%       Optional line style parameters
%

% scale so largest magnitude results in a line just 
% under the cell size
mx=max(feat(:));
s=min(cellsize(:));
scale=0.95*s./mx; 

h=zeros(numel(feat));
iline=1;
theta=2*pi*(0:size(feat,3)-1)./size(feat,3);

% adjustment for when gx,gy are transposed in orientation computation
% theta=2*pi-theta+(pi/2); 

for j=1:size(feat,1)
    for i=1:size(feat,2)
        x0=(i-0.5)*cellsize(2);
        y0=(j-0.5)*cellsize(1);
        r=scale*squeeze(feat(j,i,:));
        xs=x0+r(:).*sin(theta(:));
        ys=y0+r(:).*cos(theta(:));
        for b=1:size(feat,3)
            if(r(b)>0.5) % only draw if longer than half a pixel
                h(iline)=line([x0 xs(b)],[y0 ys(b)],varargin{:});
                iline=iline+1;
            end
        end
    end
end
h=h(1:iline-1);
end